const mongoose = require('mongoose')


const orderSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true,
        trim:true
    },
    genre:{
        type:String,
        required:true
    },

})


module.exports = mongoose.model('orderSchema',orderSchema)