const orderModel = require('../models/orderModel')



exports.orderAdd = async (req,res) => {
    try {
        const order = await new orderModel(req.body)
        const response = await order.save()

        if(response) {
            res.status(201).json({
              success:true,
              data:response
            })
        }
    }
    catch(e) {
        res.status(400).json({
            success:false,
            message:e
        })
    }
}

exports.getAllOrder = async (req,res) => {
    try {
        const order = await orderModel.find({})
        console.log(order)

        if(order) {
            res.status(200).json({
                success:true,
                data:order
            })
        }
    }
    catch(e) {
       res.status(400).json({
           success:false,
           message:e
       })
    }
}

