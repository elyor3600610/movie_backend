const {Router}  = require('express')
const { orderAdd, getAllOrder } = require('../controllers/orderController')
const router = Router()

router.post('/order/add',orderAdd)
router.get('/order/all',getAllOrder)



module.exports = router