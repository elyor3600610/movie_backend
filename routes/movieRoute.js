const {Router}  = require('express')
const {addMovie,getMovies,getMovieByCategoryId} = require('../controllers/movieController')
const router = Router()


router.post('/movie/add',addMovie)
router.put('/movie/update/:id',)
router.get('/movie/getByCategory/:id',getMovieByCategoryId)
router.get('/movie/all/',getMovies)
router.delete('/movie/delete/:id')


module.exports = router